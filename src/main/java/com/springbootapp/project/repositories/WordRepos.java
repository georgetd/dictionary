package com.springbootapp.project.repositories;

import com.springbootapp.project.beans.Word;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WordRepos extends JpaRepository<Word,Long> {


    @Query("select b from Word b where b.engversion =:engversion")
    Word findByEngversion(@Param("engversion") String engversion);


}
