package com.springbootapp.project.controllers;

import com.springbootapp.project.beans.Word;
import com.springbootapp.project.repositories.WordRepos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ControllerforArray {

    @Autowired
    WordRepos wordRepos2;

    @GetMapping("/get/array")

    public ResponseEntity<String[]> translate_array(@RequestBody String[] massive){

        for(int i = 0; i < massive.length;i++) {
            Word word = wordRepos2.findByEngversion(massive[i]);
            massive[i] = word.getRusversion();
        }

        return ResponseEntity.ok().body(massive);
    }

}
