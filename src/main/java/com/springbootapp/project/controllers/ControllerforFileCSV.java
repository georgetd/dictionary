package com.springbootapp.project.controllers;


import com.springbootapp.project.beans.Word;
import com.springbootapp.project.repositories.WordRepos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;


@RestController

public class ControllerforFileCSV {


    @Autowired

    WordRepos wordRep;

    @PostMapping("/file")

    public ResponseEntity<String> cvs (@RequestParam("file") MultipartFile multipartfile) throws IOException{
        //Данное решение по "смене" Multipartfile на File нашёл в интернете, честно говоря, не особо его понял
        //Но оно оказалось едиснтвенно рабочим
        File file = new File(multipartfile.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(multipartfile.getBytes());
        fos.close();


        Scanner scanner = new Scanner(file,"windows-1251");
        Scanner dataScanner = null;

        int index = 0;

        List<Word> words = new ArrayList<>();

        while (scanner.hasNextLine()) {
            dataScanner = new Scanner(scanner.nextLine());
            //CSV-файлы делал через Excel , там по дефолту возможен разделитель только ";"
            //Но проверял также на простых тесктовых файлах с разными разделителями - работает
            //Но для этого конечно в коде(useDelimiter("здесь"))тоже надо сменить разделить на тот , который нужен
            dataScanner.useDelimiter(";");

            Word word = new Word(null,null);

            boolean acceptionsignforeng = false;
            boolean acceptionsignforrus = false;


            while (dataScanner.hasNext()) {
                String data = dataScanner.next();
                //Так до конца и не понял , что надо было в задание о втором симовле
                //Поэтому тупо проверял и слово, и перевод на наличие постороннего знака (не буквы) через кодировку символов
                //Решение , конечно , корявое и долгое мб , но лучше не придумал:(

                if (index == 0) {
                    acceptionsignforeng = true;
                    for (int i = 0; i < data.length(); i++) {
                        char letter1 = data.charAt(i);
                        if (!(((letter1 >= 65) && (letter1 <= 90)) || ((letter1 >= 97) && (letter1 <= 122)))) {
                            acceptionsignforeng = false;
                            break;
                        }
                    }
                    if (acceptionsignforeng)
                        word.setEngversion(data);
                }
                else if (index == 1) {
                    acceptionsignforrus = true;
                    for (int i = 0; i < data.length(); i++) {
                        char letter2 = data.charAt(i);
                        System.out.println((int)letter2);
                        if (!((letter2 >= 1040) && (letter2 <= 1103))) {
                            acceptionsignforrus = false;
                            break;
                        }
                    }

                    if (acceptionsignforeng && acceptionsignforrus) {
                        word.setRusversion(data);
                        words.add(word);
                    }
                }
                index++;
            }
            index = 0;
        }
        scanner.close();

        wordRep.saveAll(words);


        return ResponseEntity.ok().body("Successful upload from file");
    }
}
