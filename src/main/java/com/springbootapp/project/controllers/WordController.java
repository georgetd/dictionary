package com.springbootapp.project.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.springbootapp.project.beans.Word;
import com.springbootapp.project.repositories.WordRepos;


import java.util.List;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;


@RestController
//@RequestMapping("/")
public class WordController {



    @Autowired
    WordRepos wordRepos;

    @GetMapping("/delete")
    public ResponseEntity<String> delete() {
        wordRepos.deleteAllInBatch();

        return ResponseEntity.ok().body("successful deleted");
    }






    //Получение перевода при написании английской версии в поисковике
    @GetMapping("/get/{engversion}")
    public ResponseEntity<String> translate(@PathVariable("engversion") String engversion) {

        return ResponseEntity.ok().body(wordRepos.findByEngversion(engversion).getRusversion());
    }

    //Получение всех элементов из словаря(Использую для проверки изменения перевода)
    @GetMapping("/get/all")
    public ResponseEntity<List<Word>> listAllPersons() {

        List<Word> persons = wordRepos.findAll();
        return ResponseEntity.ok().body(persons);
    }

    //Изменение русского перевода, используя @GetMapping, при вводе в поисковике английской версии/русской(обновленной) версии
   /* @GetMapping("/change/{engvers}/{rusvers}")

    public ResponseEntity<String> replaceword(@PathVariable("engvers") String engvers,@PathVariable("rusvers") String rusvers){
        Word word = wordRepos.findByEngversion(engvers);
        word.setRusversion(rusvers);
        wordRepos.save(word);
        return ResponseEntity.ok().body("Successful replace");
    }
*/

    //Изменение русского перевода, используя @PutMapping и @RequestBody,при вводе в поисковик английской версии,
    // и русской(обновленной) версии в request-поле, проверка в Postman
    @PutMapping("/change/{engvers}")

    public ResponseEntity<String> replaceword(@PathVariable("engvers") String engvers,@RequestBody String rusvers){
        Word word = wordRepos.findByEngversion(engvers);
        word.setRusversion(rusvers);
        wordRepos.save(word);
        return ResponseEntity.ok().body("Successful replace");
    }


}
