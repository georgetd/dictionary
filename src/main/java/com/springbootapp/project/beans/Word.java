package com.springbootapp.project.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Word {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String engversion;
    private String rusversion;

    protected Word(){
    }


    public Word(String engversion, String rusversion){
        this.engversion = engversion;
        this.rusversion = rusversion;
    }

    public String getEngversion() {

        return engversion;
    }

    public void setEngversion(String engversion) {

        this.engversion = engversion;
    }

    public String getRusversion() {

        return rusversion;
    }

    public void setRusversion(String rusversion) {

        this.rusversion = rusversion;
    }

}